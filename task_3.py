import concurrent.futures
import pickle
import requests
from urlextract import URLExtract

CONNECTIONS = 100
TIMEOUT = 5


def load_url(url, timeout):
    ans = requests.head(url, timeout=timeout)
    return ans.status_code


def main():
    with concurrent.futures.ThreadPoolExecutor(max_workers=CONNECTIONS) as executor:
        future_to_url = {executor.submit(load_url, url, TIMEOUT): url for url in urls}
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%r generated an exception: %s' % (url, exc))
            else:
                print('%r page is %d bytes' % (url, data))


if __name__ == '__main__':
    with open('messages_to_parse.dat', 'rb') as f:
        tlds = str(pickle.load(f, encoding='latin1'))
    extractor = URLExtract()
    urls = ['http://{}'.format(x) for x in extractor.gen_urls(tlds)]
    main()
