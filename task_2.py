import hashlib
import time
from shutil import copy
import os
import timeit

path = input("path:")

try:
    os.mkdir(path)
except OSError:
    print("Create directory %s failded" % path)
else:
    print("Successfully %s " % path)

uniq_hash = set()

src = input("src:")
dst = input("dst:")
files = os.listdir(src)
for fname in files:
    with open(os.path.join(src, fname), "rb") as f:
        sig = hashlib.sha256(f.read()).digest()
        if sig not in uniq_hash:
            for file in src:
                copy(os.path.join(src, fname), os.path.join(dst, fname))
                time.sleep(0.2)
                uniq_hash.add(sig)

print(timeit.timeit(setup=dst,
                    stmt=fname,
                    number=10000))
